import java.applet.Applet;
import java.awt.Color;
import java.awt.Event;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Rectangle2D.Float;

public class Main
  extends Applet
  implements Runnable
{
  public final int xMax = 50;
  public int x;
  private double y;
  private double yv;
  private double t;
  private boolean dead = true;
  private double[] yT = new double[50];
  private final Color cSnake = new Color(77, 109, 243);
  private final Color cSnakeDead = new Color(237, 28, 36);
  private final Color cSnakeEnd = new Color(this.cSnake.getRed(), this.cSnake.getGreen(), this.cSnake.getBlue(), 0);
  private final Color cSnakeDeadEnd = new Color(this.cSnakeDead.getRed(), this.cSnakeDead.getGreen(), this.cSnakeDead.getBlue(), 0);
  private Polygon sPoly;
  public final int maxP = 8;
  private double minD;
  private double maxD;
  private double maxO;
  public int pX;
  public int[] tP = new int[8];
  public int[] bP = new int[8];
  public int[] p = new int[8];
  public double[] pT = new double[8];
  public double[] pF = new double[8];
  private final Color cGround = new Color(157, 187, 97);
  private Polygon tPoly;
  private Polygon bPoly;
  public final int acu = 0;
  private int update;
  private int wait;
  public final int totalBots = 10;
  public int botRarity;
  public int botAcu;
  public int maxBots;
  private Computer[] bot = new Computer[10];
  private boolean[] botDead = new boolean[10];
  public final int width = 200;
  public final int height = 300;
  public final double acc = 0.2D;
  public final double grav = 0.1D;
  private int speed;
  private int timer;
  private boolean start = true;
  private boolean computer = false;
  private boolean down = false;
  private boolean begin = true;
  private byte blink = 0;
  private int score;
  private final Color cFor1 = new Color(0, 0, 0, 64);
  private final Color cFor2 = new Color(0, 0, 0, 0);
  private Thread th;
  private Image dbImage;
  private Graphics dbg;
  
  public void init()
  {
    setBackground(Color.black);
    

    this.x = -50;
    this.y = 150.0D;
    this.yv = 0.0D;
    this.pX = 0;
    this.t = 5.0D;
    this.score = 0;
    this.update = 0;
    this.wait = 1;
    this.timer = 0;
    this.minD = 100.0D;
    this.maxD = 100.0D;
    this.maxO = 10.0D;
    this.speed = 12;
    this.down = false;
    

    this.sPoly = new Polygon();
    for (int i = 0; i < 50; i++) {
      this.yT[i] = this.y;
    }
    this.pT[0] = 0.0D;
    this.pT[7] = 0.0D;
    

    this.tPoly = new Polygon();
    this.bPoly = new Polygon();
    

    this.tPoly.addPoint(0, 0);
    this.bPoly.addPoint(0, 300);
    for (int i = 0; i < 8; i++)
    {
      this.tP[i] = (5 + (int)(Math.random() * 25.0D));
      this.bP[i] = (295 - (int)(Math.random() * 25.0D));
      this.p[i] = (this.tP[i] + (this.bP[i] - this.tP[i]) / 2);
      

      this.tPoly.addPoint(i * 50, this.tP[i]);
      this.bPoly.addPoint(i * 50, this.bP[i]);
    }
    for (int i = 1; i < 7; i++) {
      this.pT[i] = (((this.p[(i + 1)] - this.p[i]) / 50.0D - (this.p[i] - this.p[(i - 1)]) / 50.0D) / 0.1D);
    }
    this.tPoly.addPoint(400, 0);
    this.bPoly.addPoint(400, 300);
    


    this.botRarity = 10;
    this.botAcu = 8;
    this.maxBots = 2;
    for (int i = 0; i < 10; i++) {
      this.botDead[i] = true;
    }
  }
  
  public void start()
  {
    this.th = new Thread(this);
    this.th.start();
  }
  
  public void stop()
  {
    this.th.stop();
  }
  
  private void reset()
  {
    init();
  }
  
  public boolean mouseDown(Event paramEvent, int paramInt1, int paramInt2)
  {
    if (!this.computer) {
      this.down = true;
    }
    return true;
  }
  
  public boolean mouseUp(Event paramEvent, int paramInt1, int paramInt2)
  {
    if (!this.computer) {
      this.down = false;
    }
    return true;
  }
  
  public boolean keyDown(Event paramEvent, int paramInt)
  {
    if (paramInt == 32) {
      if (this.dead)
      {
        if (this.start)
        {
          this.start = false;
          this.dead = false;
          this.computer = false;
          this.begin = false;
          
          reset();
        }
        else
        {
          this.timer = 0;
          this.start = true;
        }
      }
      else if (this.computer)
      {
        this.start = false;
        this.dead = false;
        this.computer = false;
        this.begin = false;
        
        reset();
      }
    }
    return true;
  }
  
  public void run()
  {
    this.th.setPriority(10);
    for (;;)
    {
      this.timer += 1;
      if (!this.dead)
      {
        if (this.x < 0)
        {
          this.x += 1;
          if (this.computer) {
            this.yv = 0.0D;
          }
        }
        else
        {
          this.pX -= 1;
          

          this.tPoly.translate(-1, 0);
          this.bPoly.translate(-1, 0);
          
          this.score += 1;
        }
        if (this.pX == -50)
        {
          this.pX = 0;
          

          this.tPoly = new Polygon();
          this.bPoly = new Polygon();
          

          this.tPoly.addPoint(0, 0);
          this.bPoly.addPoint(0, 300);
          for (int i = 0; i < 7; i++)
          {
            this.tP[i] = this.tP[(i + 1)];
            this.bP[i] = this.bP[(i + 1)];
            this.p[i] = this.p[(i + 1)];
            this.pT[i] = this.pT[(i + 1)];
            this.pF[i] = this.pF[(i + 1)];
            

            this.tPoly.addPoint(i * 50, this.tP[i]);
            this.bPoly.addPoint(i * 50, this.bP[i]);
          }
          double d1 = this.maxO - (this.maxO - this.tP[6]) * (this.tP[6] - this.maxO < 0.0D ? 1 : 0);
          






          double d2 = this.maxO - (this.maxO - (300.0D - this.minD - this.tP[6])) * (this.tP[6] + this.maxO > 300.0D - this.minD ? 1 : 0);
          


          this.tP[7] = (this.tP[6] + (int)(Math.random() * (d1 + d2) - d1));
          


          double d3 = this.tP[7] + this.maxD - 300.0D;
          




          this.bP[7] = (this.tP[7] + (int)this.minD + (int)(Math.random() * (d3 > 0.0D ? this.maxD - this.minD - d3 : this.maxD - this.minD)));
          

          this.p[7] = (this.tP[7] + (this.bP[7] - this.tP[7]) / 2);
          


          this.pT[6] = (((this.p[7] - this.p[6]) / 50.0D - (this.p[6] - this.p[5]) / 50.0D) / 0.1D);
          







          this.tPoly.addPoint(400, this.tP[7]);
          this.bPoly.addPoint(400, this.bP[7]);
          

          this.tPoly.addPoint(400, 0);
          this.bPoly.addPoint(400, 300);
          if ((int)(Math.random() * this.botRarity) == 0) {
            for (int k = 0; k < this.maxBots; k++) {
              if (this.botDead[k])
              {
                this.botDead[k] = false;
                this.bot[k] = new Computer(this, k, 1.0D, this.botAcu);
                break;
              }
            }
          }
        }
        if (this.computer)
        {
          this.update += 1;
          if (this.update == this.wait)
          {
            this.wait = ((int)(Math.random() * 0.0D) + 0 + 1);
            

            this.update = 0;
          }
          if (this.pX == -25) {
            this.t = this.pT[2];
          }
          if ((-this.pX <= Math.abs(this.t) / 2.0D) || (-this.pX >= 50.0D - Math.abs(this.t) / 2.0D))
          {
            if (this.t < 0.0D) {
              this.down = true;
            } else {
              this.down = false;
            }
          }
          else if (this.update == 0) {
            if (((this.yv > (this.p[2] - this.p[1]) / 50.0D) || (this.y + this.yv * (50 + this.pX) > this.p[2])) && (this.y + this.yv * (50 + this.pX) > this.p[2])) {
              this.down = true;
            } else {
              this.down = false;
            }
          }
        }
        if (this.down) {
          this.yv -= 0.2D;
        }
        this.yv += 0.1D;
        

        this.y += this.yv;
        for (int j = 49; j > 0; j--) {
          this.yT[j] = this.yT[(j - 1)];
        }
        this.yT[0] = this.y;
        

        this.sPoly = new Polygon();
        for (int j = 0; j < 50; j++) {
          this.sPoly.addPoint(49 - j + this.x, (int)this.yT[j] - 2);
        }
        for (int j = 0; j < 50; j++) {
          this.sPoly.addPoint(this.x + j, (int)this.yT[(49 - j)] + 3);
        }
        if ((this.y - 2.0D < this.tP[1] + (this.tP[2] - this.tP[1]) / 50.0D * -this.pX) || (this.y + 2.0D > this.bP[1] + (this.bP[2] - this.bP[1]) / 50.0D * -this.pX))
        {
          this.dead = true;
          if (this.computer) {
            this.timer = 0;
          }
        }
        for (int j = 0; j < this.maxBots; j++) {
          if (!this.botDead[j]) {
            this.bot[j].run();
          }
        }
      }
      else if ((this.timer == 400) && ((this.start) || (!this.dead)))
      {
        this.dead = false;
        this.computer = true;
        this.start = true;
        if (!this.begin) {
          reset();
        }
        this.begin = false;
      }
      if (this.timer % 50 == 0)
      {
        this.blink = ((byte)(this.blink + 1));
        if (this.blink == 4) {
          this.blink = 0;
        }
      }
      if (this.maxD > 40.0D) {
        this.maxD = (200.0D - this.score / 3800.0D * 120.0D);
      }
      if (this.minD > 30.0D) {
        this.minD = (100.0D - this.score / 3800.0D * 60.0D);
      }
      if (this.maxO < 100.0D) {
        this.maxO = (50.0D + this.score / 3800.0D * 200.0D);
      }
      if (this.maxBots < 10) {
        this.maxBots = ((int)(2.0D + this.score / 3800.0D * 16.0D));
      }
      if (this.botAcu > 0) {
        this.botAcu = ((int)(8.0D - this.score / 3800.0D * 14.0D));
      }
      if (this.botRarity > 6) {
        this.botRarity = ((int)(10.0D - this.score / 3800.0D * 3.0D));
      }
      repaint();
      try
      {
        Thread.sleep(this.speed);
      }
      catch (InterruptedException localInterruptedException) {}
      Thread.currentThread().setPriority(10);
    }
  }
  
  public void killBot(int paramInt)
  {
    this.botDead[paramInt] = true;
  }
  
  public void paint(Graphics paramGraphics)
  {
    Graphics2D localGraphics2D = (Graphics2D)paramGraphics;
    RenderingHints localRenderingHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    localGraphics2D.setRenderingHints(localRenderingHints);
    GradientPaint localGradientPaint;
    if (!this.dead) {
      localGradientPaint = new GradientPaint(this.x, 0.0F, this.cSnakeEnd, 50 + this.x, 0.0F, this.cSnake);
    } else {
      localGradientPaint = new GradientPaint(this.x, 0.0F, this.cSnakeDeadEnd, 50 + this.x, 0.0F, this.cSnakeDead);
    }
    localGraphics2D.setPaint(localGradientPaint);
    localGraphics2D.fill(this.sPoly);
    for (int i = 0; i < this.maxBots; i++) {
      if (!this.botDead[i]) {
        this.bot[i].paint(paramGraphics, localGraphics2D);
      }
    }
    paramGraphics.setColor(this.cGround);
    paramGraphics.fillPolygon(this.tPoly);
    paramGraphics.fillPolygon(this.bPoly);
    






    Rectangle2D.Float localFloat1 = new Rectangle2D.Float(100.0F, 0.0F, 100.0F, 300.0F);
    

    localGradientPaint = new GradientPaint(100.0F, 0.0F, this.cFor1, 200.0F, 0.0F, Color.black);
    

    localGraphics2D.setPaint(localGradientPaint);
    localGraphics2D.fill(localFloat1);
    

    Rectangle2D.Float localFloat2 = new Rectangle2D.Float(0.0F, 0.0F, 100.0F, 300.0F);
    

    localGradientPaint = new GradientPaint(0.0F, 0.0F, this.cFor2, 100.0F, 0.0F, this.cFor1);
    localGraphics2D.setPaint(localGradientPaint);
    localGraphics2D.fill(localFloat2);
    





    paramGraphics.setColor(Color.yellow);
    

    paramGraphics.drawString("Score: " + this.score, 2, 12);
    if (this.blink > -1) {
      if ((!this.start) && (this.dead))
      {
        if (this.blink == 0) {
          paramGraphics.drawString("Game Over", 2, 24);
        } else if (this.blink == 2) {
          paramGraphics.drawString("Press Space to Continue", 2, 24);
        }
      }
      else if (((this.start) || (this.dead)) && (this.blink % 2 == 0)) {
        paramGraphics.drawString("Press Space to Start", 2, 24);
      }
    }
  }
  
  public void update(Graphics paramGraphics)
  {
    if (this.dbImage == null)
    {
      this.dbImage = createImage(200, 300);
      this.dbg = this.dbImage.getGraphics();
    }
    this.dbg.setColor(Color.black);
    this.dbg.fillRect(0, 0, 200, 300);
    
    this.dbg.setColor(getForeground());
    paint(this.dbg);
    
    paramGraphics.drawImage(this.dbImage, 0, 0, this);
  }
}
