import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;

public class Computer
{
  private final int nTrail;
  private final double xV;
  private double x;
  private double y;
  private double yv;
  private double dx;
  private double t;
  private boolean dead;
  private double[] yT;
  private final Color cSnake = new Color(255, 242, 0);
  private final Color cSnakeDead = new Color(237, 28, 36);
  private final Color cSnakeEnd = new Color(this.cSnake.getRed(), this.cSnake.getGreen(), this.cSnake.getBlue(), 0);
  private final Color cSnakeDeadEnd = new Color(this.cSnakeDead.getRed(), this.cSnakeDead.getGreen(), this.cSnakeDead.getBlue(), 0);
  private Polygon sPoly;
  private int pX;
  private int cS;
  private final int acu;
  private int update;
  private int wait;
  private final double dD;
  private final double dV;
  private int botNumber;
  private final double acc;
  private final double grav;
  private boolean down;
  private Main main;
  
  public Computer(Main paramMain, int paramInt1, double paramDouble, int paramInt2)
  {
    this.main = paramMain;
    this.botNumber = paramInt1;
    this.acu = paramInt2;
    this.xV = paramDouble;
    

    this.dV = (1.0D + this.xV);
    

    this.dD = (50.0D / this.dV);
    

    this.acc = (this.dV * 0.2D);
    this.grav = (this.dV * 0.1D);
    

    this.nTrail = ((int)this.dD);
    this.yT = new double[this.nTrail];
    

    this.x = 0.0D;
    this.dx = 0.0D;
    this.pX = 1;
    this.y = this.main.p[0];
    this.yv = ((this.main.p[1] - this.main.p[0]) / this.dD);
    this.t = 0.0D;
    this.dead = false;
    this.down = false;
    this.update = 0;
    this.wait = 1;
    this.cS = 0;
    for (int i = 0; i < this.nTrail; i++) {
      this.yT[i] = this.y;
    }
  }
  
  public void run()
  {
    if (!this.dead)
    {
      this.x += this.xV;
      this.pX -= 1;
      

      this.main.getClass();
      if (this.x > 200.0D + this.nTrail * this.dV) {
        this.main.killBot(this.botNumber);
      }
      if (this.pX == -(int)this.dD)
      {
        this.pX = 0;
        

        this.cS += 1;
      }
      if (this.main.pX == 0) {
        if ((this.pX == 0) && (this.x > this.xV)) {
          this.cS -= 1;
        }
      }
      this.update += 1;
      if (this.update == this.wait)
      {
        this.wait = ((int)(Math.random() * this.acu) + this.acu / 2 + 1);
        

        this.update = 0;
      }
      if (this.pX == -(int)this.dD / 2) {
        this.t = (((this.main.p[(this.cS + 2)] - this.main.p[(this.cS + 1)]) / this.dD - (this.main.p[(this.cS + 1)] - this.main.p[this.cS]) / this.dD) / this.grav);
      }
      if ((-this.pX <= Math.abs(this.t) / 2.0D) || (-this.pX >= this.dD - Math.abs(this.t) / 2.0D))
      {
        if (this.t < 0.0D) {
          this.down = true;
        } else {
          this.down = false;
        }
      }
      else if (this.update == 0) {
        if (((this.yv > (this.main.p[(this.cS + 1)] - this.main.p[this.cS]) / this.dD) || (this.y + this.yv * (this.dD + this.pX) > this.main.p[(this.cS + 1)])) && (this.y + this.yv * (this.dD + this.pX) > this.main.p[(this.cS + 1)])) {
          this.down = true;
        } else {
          this.down = false;
        }
      }
      if (this.down) {
        this.yv -= this.acc;
      }
      this.yv += this.grav;
      

      this.y += this.yv;
      for (int i = this.nTrail - 1; i > 0; i--) {
        this.yT[i] = this.yT[(i - 1)];
      }
      this.yT[0] = this.y;
      

      this.sPoly = new Polygon();
      for (int i = 0; i < this.nTrail; i++) {
        this.sPoly.addPoint((int)(this.x - i * this.dV), (int)this.yT[i] - 2);
      }
      for (int i = 0; i < this.nTrail; i++) {
        this.sPoly.addPoint((int)(this.x - (this.nTrail - 1 - i) * this.dV), (int)this.yT[(this.nTrail - 1 - i)] + 3);
      }
      if ((this.y < this.main.tP[this.cS] + (this.main.tP[(this.cS + 1)] - this.main.tP[this.cS]) / this.dD * (-this.pX - 1)) || (this.y > this.main.bP[this.cS] + (this.main.bP[(this.cS + 1)] - this.main.bP[this.cS]) / this.dD * (-this.pX - 1)))
      {
        this.dead = true;
        

        this.dx = this.x;
      }
    }
    if (this.dead)
    {
      this.x -= 1.0D;
      

      this.dx -= 0.5D;
      

      this.sPoly.translate(-1, 0);
      if (this.x < this.dx - this.nTrail * this.dV) {
        this.main.killBot(this.botNumber);
      }
    }
  }
  
  public void paint(Graphics paramGraphics, Graphics2D paramGraphics2D)
  {
    GradientPaint localGradientPaint;
    if (!this.dead) {
      localGradientPaint = new GradientPaint((int)(this.x - this.nTrail * this.dV), 0.0F, this.cSnakeEnd, (int)this.x, 0.0F, this.cSnake);
    } else {
      localGradientPaint = new GradientPaint((int)(this.dx - this.nTrail * this.dV), 0.0F, this.cSnakeDeadEnd, (int)this.dx, 0.0F, this.cSnakeDead);
    }
    paramGraphics2D.setPaint(localGradientPaint);
    paramGraphics2D.fill(this.sPoly);
  }
}
