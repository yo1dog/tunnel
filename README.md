A simple 2D scroller in which you must click your mouse to keep your line from touching the top and bottom of the tunnel. The difficulty progresses with the level.

## Instructions

Click on the game and press space to start. Click the mouse to make the snake rise.

---------------------------------------

 - [View Applet](http://applets.awesomebox.net/tunnel.html)

---------------------------------------

[![Screenshot](https://s3.amazonaws.com/mike-projects/Tunnel/Tunnel.png)](https://s3.amazonaws.com/mike-projects/Tunnel/Tunnel.png)